'use strict';

var climber = climber || {};

climber.Settings = function(){};
climber.Settings.prototype = {
	create: function(){
		climber.createAutoScrollBackground();
		this.createTitle();
		this.buttonsGroup = this.game.add.group();
		this.buttonsGroup.onChildInputUp.add( climber.buttonsUpEfect, this );
		this.buttonsGroup.onChildInputDown.add( climber.buttonsDownEfect, this );
		this.createSettingsMenu();
		this.createBackButton();
	},
	createTitle: function(){
		this.title = this.game.add.text( this.game.world.centerX, this.game.camera.y, "Settings", styles.stateTitle );
		this.title.fixedToCamera = true;
		this.title.anchor.set( 0.5, 0);
	},
	createSettingsMenu: function(){
		var buttonSize = this.game.buttonSize / 2;
		var buttonsPosition = this.title.y + this.title.height + 100;
		this.music = this.game.add.text( this.game.world.centerX, buttonsPosition, "Music", styles.settings );
		this.music.fixedToCamera = true;
		this.music.anchor.set( 1, 1);

		this.musicIcon = this.game.add.button( this.game.world.centerX, buttonsPosition, "buttons" );
		this.musicIcon.frame = 7;
		this.musicIcon.blendMode = (this.game.settings.music) ? PIXI.blendModes.NORMAL : PIXI.blendModes.DARKEN;
		this.musicIcon.height = buttonSize;
		this.musicIcon.width = buttonSize;
		this.musicIcon.fixedToCamera = true;
		this.musicIcon.anchor.set( -1, 1);

		this.musicIcon.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.settings.music = !this.game.settings.music;
					if ( this.game.settings.music ){
						this.game.audio.game.loopFull( 0.5 );
						sprite.blendMode = PIXI.blendModes.NORMAL;
					}
					else{
						this.game.audio.game.stop();
						sprite.blendMode = PIXI.blendModes.DARKEN;
					}
					this.saveSettings();
				}
		}, this);

		this.effects = this.game.add.text( this.game.world.centerX, buttonsPosition, "Sfx", styles.settings );
		this.effects.fixedToCamera = true;
		this.effects.anchor.set( 1, -1 );

		this.effectsIcon = this.game.add.button( this.game.world.centerX, buttonsPosition, "buttons" );
		this.effectsIcon.frame = 7;
		this.effectsIcon.blendMode = (this.game.settings.effects) ? PIXI.blendModes.NORMAL : PIXI.blendModes.DARKEN;
		this.effectsIcon.height = buttonSize;
		this.effectsIcon.width = buttonSize;
		this.effectsIcon.fixedToCamera = true;
		this.effectsIcon.anchor.set( -1, -1);

		this.effectsIcon.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.settings.effects = !this.game.settings.effects;
					if ( this.game.settings.effects ){
						sprite.blendMode = PIXI.blendModes.NORMAL;
					}
					else{
						sprite.blendMode = PIXI.blendModes.DARKEN;
					}
					this.saveSettings();
				}
		}, this);

	},
	saveSettings: function(){
		localStorage.setItem( this.game.settings.FILENAME, JSON.stringify( this.game.settings ) );
	},
	createBackButton: function(){
		this.backButton = this.game.add.button( this.game.world.centerX, this.game.camera.y + this.game.height, 'buttons' );
		this.backButton.fixedToCamera = true;
		this.backButton.frame = 0;
		this.backButton.width = this.game.buttonSize;
		this.backButton.height = this.game.buttonSize;
		this.backButton.anchor.set( 0.5, 1.5 );

		this.backButton.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.state.start(climber.game.states.MAINMENU );
				}
			}, this	);

		this.buttonsGroup.add( this.backButton );
	}
}
