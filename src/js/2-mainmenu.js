'use strict';

var climber = climber || {};

climber.MainMenu = function(){};
climber.MainMenu.prototype = {
	create: function(){
		climber.createAutoScrollBackground();
		climber.loadGameAudio();
		this.createTitle();
		this.createMenu();
		this.createVersion();
	},
	render: function(){
	},
	createTitle: function(){

		this.titleGroup = this.game.add.group();
		var logo = this.game.add.sprite( this.game.world.centerX, this.game.camera.quarterHeigth, 'logo');
		logo.width = logo.width*ratio;
		logo.height = logo.height*ratio;
		logo.anchor.set( 0.5);
		this.titleGroup.add( logo);

		var title  = 'Ninja';
		var ninjaTitle = this.game.add.text( this.game.world.centerX, logo.y + (logo.height/1.5), title, styles.ninjaTitle );
		ninjaTitle.anchor.set( 0.5);
		this.titleGroup.add( ninjaTitle);

		title = 'Climber';
		var climberTitle = this.game.add.text( this.game.world.centerX, ninjaTitle.y+(ninjaTitle.height/3) , title, styles.climberTitle );
		climberTitle.anchor.set( 0.3 );
		this.titleGroup.add( climberTitle);
	},
	createMenu: function(){
		this.menu = {};
		this.menu.group = this.game.add.group();

		var climberTitle = this.titleGroup.children[ this.titleGroup.length-1 ];

		var frame = 3;
		this.menu.play = this.game.add.button( this.game.world.centerX, climberTitle.y + climberTitle.height, 'buttons');
		this.menu.play.frame = frame;
		this.menu.play.width = this.game.buttonSize + 40;
		this.menu.play.height = this.game.buttonSize + 40;
		this.menu.play.anchor.set( 0.5, 0 );
		this.menu.play.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.state.start(climber.game.states.GAME);
				}
			}, this);
		this.menu.group.add( this.menu.play );

		var buttonsLine2 = this.menu.play.y + 60;
		frame = 8;
		this.menu.scores = this.game.add.button( this.game.world.centerX - this.menu.play.width, buttonsLine2, 'buttons')
		this.menu.scores.frame = frame;
		this.menu.scores.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.state.start(climber.game.states.SCORES);
				}
			}, this);
		this.menu.scores.width = this.game.buttonSize;
		this.menu.scores.height = this.game.buttonSize;
		this.menu.scores.anchor.set( 0.5, 0 );
		this.menu.group.add( this.menu.scores );

		frame = 5;
		this.menu.settings = this.game.add.button( this.game.world.centerX + this.menu.play.width, buttonsLine2, 'buttons');
		this.menu.settings.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.state.start(climber.game.states.SETTINGS );
				}
			}, this);
		this.menu.settings.frame = frame;
		this.menu.settings.width = this.game.buttonSize;
		this.menu.settings.height = this.game.buttonSize;
		this.menu.settings.anchor.set( 0.5, 0 );
		this.menu.group.add( this.menu.settings );

		this.menu.group.onChildInputUp.add( climber.buttonsUpEfect, this);
		this.menu.group.onChildInputDown.add( climber.buttonsDownEfect, this );
	},
	createVersion: function(){
		let versionText = this.game.add.text( this.game.world.width-20, this.game.world.height-20, `v. ${version}`, styles.version );
		versionText.anchor.set( 1 );
	}
}
