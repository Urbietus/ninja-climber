'use strict';

var climber = climber || {};

climber.Game = function(){};
climber.Game.prototype = {
	create: function(){
		this.game.audio.game.volume = 0.1;

		this.game.world.setBounds( 0, 0, this.game.width, this.game.height);
		this.game.physics.startSystem( Phaser.Physics.ARCADE );
		this.game.physics.arcade.gravity.y = 500;

		this.game.cameraYMin = 99999;
    	this.game.platformYMin = 99999;

		this.background = this.game.add.tileSprite(0,0, this.game.width, this.game.height, 'sky');
		this.background.tilePosition.y = 0;
		this.background.fixedToCamera = true;

		this.createGround();
		this.createCountDown();
		this.createScores();
		this.createTimeToAddTimer();
		this.createNinja();

		this.started = false;

		this.game.camera.follow( this.ninja);
		this.game.input.onTap.add( this.onTap, this);
		this.game.physics.arcade.collide( this.ninja, this.ground);

	},
	update: function(){
		this.world.setBounds( 0, -this.ninja.yChange, this.world.width, this.game.height + this.ninja.yChange );
		this.background.tilePosition.y = -( this.camera.y * 0.8 );
		var ninjaY = Math.abs( this.ninja.y - this.ninja.yStart );
		this.ninja.yChange = Math.max( this.ninja.yChange, ninjaY );
		this.scores.updateValues( ninjaY );
		this.game.physics.arcade.collide( this.ninja, this.ground,   this.ninja.onTouchGround );
		this.game.physics.arcade.overlap( this.timeToAddTimer.elementsGroup, this.ground, this.timeToAddTimer.onTouchGround, null, this );
		this.game.physics.arcade.overlap( this.ninja, this.timeToAddTimer.elementsGroup,  this.ninja.ontimeFallTextCollide );

	},
	createCountDown: function(){
		var countdownInitialValueMs = 10000;
		var padding = 10;
		this.game.countdown = this.game.add.text( this.game.world.width-padding, padding, `${countdownInitialValueMs/1000}s`, styles.countdown);
		this.game.countdown.anchor.set(1, 0);
		this.game.countdown.fixedToCamera = true;
		this.game.countdown.value = countdownInitialValueMs;
		this.game.countdown.loopValue = 100;

		this.game.countdown.timer = this.game.time.create( false);
		this.game.countdown.timer.loopEvent = this.game.countdown.timer.loop( this.game.countdown.loopValue, this.onCountDownLoop, this);
		this.game.countdown.timer.onComplete.add( this.onFinishTime, this);
	},
	onCountDownLoop: function(){
		let rest = this.game.countdown.value - this.game.countdown.loopValue;
		this.game.countdown.value = rest;
		this.game.countdown.text = `${this.game.countdown.value/1000}s`;
		if(this.game.countdown.value <= 0){
			this.game.countdown.timer.stop();
		}else if( this.game.countdown.value <= 5000 ){
			this.game.countdown.fill = styles.countdown.dangerColor;
		}
		else{
			this.game.countdown.fill = styles.countdown.okColor;
		}
	},
	createNinja: function(){
		this.ninja = this.game.add.sprite(this.game.world.centerX,this.game.world.height-this.ground.sprites.height, 'ninja');
		this.ninja.anchor.set(0.5, 1);
		this.ninja.width = 82.5*ratio;
		this.ninja.height = 116.5*ratio;
		this.ninja.animations.names = {
			stop: 'stop',
			jumpLeft: 'jump-left',
			jumpRight: 'jump-right',
			sliceLeft: 'slice-left',
			sliceRight: 'slice-right',
			glideRight: 'glide-right',
			glideLeft: 'glide-left'
		};
		var sliceAnimationSpeed = 10;
		this.ninja.animations.add( this.ninja.animations.names.stop,		[0,1,2,3,4,5,6,7,8,9],				this.game.config.fps, true );
		this.ninja.animations.add( this.ninja.animations.names.jumpLeft,	[10,11,12,13,14,15,16,17,18,19],	this.game.config.fps, false );
		this.ninja.animations.add( this.ninja.animations.names.jumpRight,	[20,21,22,23,24,25,26,27,28,29],	this.game.config.fps, false );
		this.ninja.animations.add( this.ninja.animations.names.sliceLeft, 	[30,31,32,33,34,35,36,37,38,39],	sliceAnimationSpeed, true );
		this.ninja.animations.add( this.ninja.animations.names.sliceRight,	[40,41,42,43,44,45,46,47,48,49],	sliceAnimationSpeed, true );
		this.ninja.animations.add( this.ninja.animations.names.glideLeft,	[50,51,52,53,54,55,56,57,58,59],	this.game.config.fps, true );
		this.ninja.animations.add( this.ninja.animations.names.glideRight,	[60,61,62,63,64,65,66,67,68,69],	this.game.config.fps, true );
		this.game.physics.arcade.enableBody( this.ninja );
		this.ninja.yChange = 0;
		this.ninja.yStart = this.ninja.y;
		this.ninja.body.collideWorldBounds = true;
		this.ninja.body.bounce.setTo(0);
		this.ninja.animations.play( this.ninja.animations.names.stop);
		this.ninja.jump = {};
		this.ninja.jump.directions = {
			L: 'left',
			R: 'right'
		}
		this.ninja.jump.nextDirection = this.ninja.jump.directions.R;
		this.ninja.isJumping = false;
		this.ninja.isSlicing = false;
		this.ninja.isFalling = false;
		this.ninja.isOnGround = true;

		this.ninja.onTouchGround = function( ninja, ground){
			if( (!ninja.isJumping || ninja.isSlicing || ninja.isFalling) && !ninja.isOnGround ){
				climber.playAudio( ninja.game.audio.keys.LAND );
				if( ninja.isJumping ){
					ninja.isJumping = false;
				}
				if( ninja.isSlicing ){
					ninja.isSlicing = false;
					climber.stopAudio( ninja.game.audio.keys.SLICING );
				}
				if( ninja.isFalling ){
					ninja.isFalling = false;
					climber.playAudio( ninja.game.audio.keys.CLOSEPARACA );
				}
				if( !ninja.isOnGround ){
					ninja.isOnGround = true;
				}
				ninja.body.velocity.y = 0;
				ninja.body.velocity.x = 0;
				ninja.animations.play( ninja.animations.names.stop );
			}
		};

		this.ninja.onWorldBounds = function( ninja, up, down, left, right ){
			if( ! ninja.isSlicing ){
				climber.playAudio( ninja.game.audio.keys.SLICING, true );
				ninja.body.velocity.y = 0;
				ninja.isSlicing = true;
				ninja.isFalling = false;
				ninja.isJumping = false;
				if( right ){
					ninja.jump.nextDirection = this.ninja.jump.directions.L;
					ninja.animations.play( ninja.animations.names.sliceRight );
				}
				else if( left){
					ninja.jump.nextDirection = this.ninja.jump.directions.R;
					ninja.animations.play( ninja.animations.names.sliceLeft);
				}
			}
		}
		this.ninja.body.onWorldBounds = new Phaser.Signal();
		this.ninja.body.onWorldBounds.add( this.ninja.onWorldBounds, this);

		this.ninja.ontimeFallTextCollide = function( ninja, timeFallText ){
			if( ! timeFallText.collided ){
				timeFallText.collided = true;
				( timeFallText.msValue > 0 ) ? climber.playAudio( ninja.game.audio.keys.TIMEADD ) : climber.playAudio( ninja.game.audio.keys.TIMEREMOVE );
				var tween = ninja.game.add.tween( timeFallText ).to( { x: ninja.game.countdown.x-(ninja.game.countdown.width/2), y: ninja.game.countdown.y }, 150, Phaser.Easing.Default, true);
				// console.log( `CountDownVal -> ${ninja.game.countdown.value} - TimeMS -> ${timeFallText.msValue}`);
				ninja.game.countdown.value = ninja.game.countdown.value + timeFallText.msValue;
				tween.onComplete.add( ()=>{
					timeFallText.kill();
				}, this);
			}
		}
	},
	createGround: function(){
		this.ground = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
		this.ground.sprites = {};
		this.ground.sprites.width = 48*ratio;
		this.ground.sprites.height = 48*ratio;
		this.ground.sprites.yPosition = this.game.height - this.ground.sprites.height;
		var groundSpritesNumbers = Math.ceil( this.game.width / this.ground.sprites.width);
		for (var i = 0; i < groundSpritesNumbers; i++) {
			var groundSprite = this.ground.create(this.ground.sprites.width * i, this.ground.sprites.yPosition, 'platform', 1);
			groundSprite.width = this.ground.sprites.width;
			groundSprite.height = this.ground.sprites.height;
			groundSprite.body.inmovable = true;
			groundSprite.body.allowGravity = false;
			groundSprite.body.collideWorldBounds = true;
		}
	},
	createScores: function(){
		this.scores = {};
		var roundMax = parseFloat(0);
		this.scores.roundMax = this.game.add.text( 10, 10, `Max: ${roundMax} m`, styles.score );
		this.scores.roundMax.fixedToCamera = true;
		this.scores.roundMax.value = roundMax;
		this.scores.roundMax.updateValue = function( value ){
			this.value = value;
			this.text = `Max: ${value} m`;
		}

		this.scores.actualValue = this.game.add.text( 10, this.scores.roundMax.y+this.scores.roundMax.height, `${roundMax} m`, styles.score );
		this.scores.actualValue.fixedToCamera = true;
		this.scores.actualValue.value = 0;
		this.scores.actualValue.updateValue = function( value){
			this.value = value;
			this.text = `${value} m`;
		};

		this.scores.destroy = function(){
			this.roundMax.destroy();
			this.actualValue.destroy();
		}

		this.scores.updateValues = function( ninjaY){
			var value = parseFloat( (Math.round( ninjaY / 20 ) / 10));
			this.actualValue.updateValue( value );
			if( this.roundMax.value < value){
				this.roundMax.updateValue( value);
			}
		};
	},
	onTap: function( pointer, doubleTap ){
		if( !this.started ){
			this.game.countdown.timer.start();
			this.timeToAddTimer.start();
			this.started = true;
		}
		if( this.ninja.isOnGround ){
			this.ninja.isOnGround = false;
		}
		climber.stopAudio( this.game.audio.keys.SLICING );

		var jumpRight = (this.ninja.jump.nextDirection == this.ninja.jump.directions.R);
		if( ! this.ninja.isJumping && ! this.ninja.isFalling ){
			climber.playAudio( this.game.audio.keys.JUMP );
			this.ninja.isJumping = true;
			this.ninja.isSlicing = false;
			var jumpValue = 100;
			var position = { x: 0, y: this.ninja.body.y - jumpValue};
			if( jumpRight ){
				this.ninja.animations.play( this.ninja.animations.names.jumpRight );
				position.x = this.game.world.width - this.ninja.body.halfWidth;
			}
			else{
				this.ninja.animations.play( this.ninja.animations.names.jumpLeft );
				position.x = 0 + this.ninja.body.halfWidth;
			}
			var speedFramePerSecond = 90;
			var left2RideDurationMs = 300;
			var xDistance = Math.abs( position.x - this.ninja.x);
			var durationMs = left2RideDurationMs * xDistance / this.game.world.width;
			this.game.physics.arcade.moveToXY( this.ninja, position.x, position.y, speedFramePerSecond, durationMs);
		}
		else{
			climber.playAudio( this.game.audio.keys.OPENPARACA );
			this.ninja.body.velocity.y = 500;
			var velocity, animationName;
			if( jumpRight ){
				velocity = 50;
				animationName = this.ninja.animations.names.glideRight;
			}else{
				velocity = -50;
				animationName = this.ninja.animations.names.glideLeft;
			}
			this.ninja.body.velocity.x = velocity;
			this.ninja.animations.play( animationName );
			this.ninja.isJumping = false;
			this.ninja.isSlicing = false;
			this.ninja.isFalling = true;
		}
	},
	onFinishTime: function(){
		this.timeToAddTimer.stop();
		this.timeToAddTimer.elementsGroup.destroy();

		this.game.countdown.destroy();

		this.ninja.body.velocity = [0,0];
		this.ninja.animations.stop( false, false);

		climber.stopAudio( this.game.audio.keys.SLICING );

		this.game.input.onTap.remove( this.onTap, this );

		var finishText = this.game.add.text( this.game.world.centerX, this.game.camera.y + this.game.camera.quarterHeigth, 'Finish!!!', styles.finish);
		finishText.anchor.set(0.5, 0);
		var maxValue = this.scores.roundMax.value;

		var value = this.game.add.text( this.game.world.centerX, finishText.y + finishText.height, `${this.scores.roundMax.value} m`, styles.finish);
		value.anchor.set( 0.5, 0 );

		var buttonsLine = value.y + value.height;
		var buttonsGroup = this.game.add.group();

		var frame = 0;
		var backMenu = this.game.add.button( this.game.world.centerX, buttonsLine, 'buttons');
		backMenu.frame = frame;
		backMenu.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.state.start(this.game.states.MAINMENU);
				}
			}, this );
		backMenu.width = this.game.buttonSize;
		backMenu.height = this.game.buttonSize;
		backMenu.anchor.set( 1.5, 0 );
		buttonsGroup.add( backMenu );

		frame = 4;
		var restartMenu = this.game.add.button( this.game.world.centerX, buttonsLine, 'buttons');
		restartMenu.frame = 4;
		restartMenu.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.state.restart(this.game.states.Game);
				}
			}, this);
		restartMenu.width = this.game.buttonSize;
		restartMenu.height = this.game.buttonSize;
		restartMenu.anchor.set( -0.5, 0 );
		buttonsGroup.add( restartMenu );

		var saveRecordButton = this.game.add.button( this.game.world.centerX, buttonsLine+restartMenu.height, 'buttons');
		saveRecordButton.frame = 8;
		saveRecordButton.width = this.game.buttonSize;
		saveRecordButton.height = this.game.buttonSize;
		saveRecordButton.anchor.set( 0.5 );
		saveRecordButton.onInputUp.add(
			(sprite, pointer, isOver) =>{
				if( isOver){
					this.game.state.start( this.game.states.SCORES, true, false, this.scores.roundMax.value );
				}
			}
		);
		buttonsGroup.add( saveRecordButton);

		buttonsGroup.onChildInputUp.add( climber.buttonsUpEfect, this);
		buttonsGroup.onChildInputDown.add( climber.buttonsDownEfect, this );
	},
	createTimeToAddTimer: function(){
		this.timeToAddTimer = this.game.time.create( false);
		this.timeToAddTimer.elementsGroup = this.game.add.physicsGroup( Phaser.Physics.ARCADE );
		this.timeToAddTimer.onTouchGround = function( time, ground){
			time.kill();
		};
		this.timeToAddTimer.loop( 1000, ()=>{

			var xPadding = 60;
			var x = this.game.rnd.integerInRange( 5, this.game.world.width-xPadding);
			var y = -this.ninja.yChange;

			var rndIndex = this.game.rnd.integerInRange( 0, timeFall.values.length-1);
			var rndSize = this.game.rnd.integerInRange( timeFall.sizes.min, timeFall.sizes.max);
			var timeFallValue = timeFall.values[ rndIndex ];
			var text = `${(timeFallValue.value>0)?'+':''}${timeFallValue.value}s`;
			var fallStyle = Object.assign({}, styles.timeFall,{	fontSize: rndSize,	fill: timeFallValue.color	});
			var timeFallText = this.game.add.text( 0, 0, text, fallStyle );
			var timeFallSprite = this.timeToAddTimer.elementsGroup.create(x, y, null);
			this.game.physics.enable( timeFallSprite, Phaser.Physics.ARCADE );
			timeFallSprite.enableBody = true;
			timeFallSprite.addChild( timeFallText );
			timeFallSprite.msValue = ( timeFallValue.value * 1000 );
			timeFallSprite.body.gravity.y = this.game.rnd.integerInRange( 100, 2000);

			this.delay = this.game.rnd.integerInRange( timeFall.delay.min, timeFall.delay.max);
		}, this);
	}
}
