'use strict';

var climber = climber || {};

climber.Boot = function(){};
climber.Boot.prototype = {
	preload: function(){
		this.game.config = {};
	},
	create: function(){
		this.game.stage.backgroundColor = '#122665';

		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		if( ! this.game.device.desktop ){
			var targetWidth = window.innerWidth * window.devicePixelRatio;
			var targetHeight = window.innerHeight * window.devicePixelRatio;
			this.game.scale.setGameSize( targetWidth, targetHeight );
			this.game.scale.forcePortrait = true;
			if ( this.game.device.cordova ) {
				this.game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
				this.game.scale.refresh();
			}
		}
		this.game.config.fps = 30;
		this.state.start( this.game.states.PRELOAD );
	}
}
