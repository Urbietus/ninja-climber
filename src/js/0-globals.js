'use strict';
const DEBUG = false;
const version = '1.1';
const targetWidth = 540 , targetHeight = 960;
const mainFontName = 'Rock Salt';
const secondFontName = 'Fugaz One';
const fontColor = '#ffffff';
const fontStrokeColor = '#14143f';
const ratio = (window.devicePixelRatio>1) ? (window.innerHeight/window.innerWidth) : 1;
let styles = {
	loadBar: {
		font: '32px Arial',
		fill: fontColor,
		align: 'center'
	},
	ninjaTitle: {
		font: secondFontName,
		fill: fontColor,
		stroke: fontStrokeColor,
		fontSize: 110*ratio,
		strokeThickness: 7*ratio
	},
	climberTitle: {
		font: secondFontName,
		fill: fontStrokeColor,
		stroke: fontColor,
		fontSize: 90*ratio,
		strokeThickness: 6*ratio
	},
	version: {
		font: secondFontName,
		fontSize: 20*ratio,
		strokeThickness: 4*ratio,
		fill: fontColor,
		stroke: fontStrokeColor
	},
	countdown: {
		font: secondFontName,
		fontSize: 35*ratio,
		fill: '#008F3B',
		stroke: '#001708',
		strokeThickness: 2*ratio,
		align: 'right',
		boundsAlignH: 'right',
		okColor: '#008F3B',
		dangerColor:'#DA0000'
	},
	score: {
		font: secondFontName,
		fill: "#ffffff",
		stroke: "#535353",
		fontSize: 22*ratio,
		strokeThickness: 2*ratio,
		align: 'center'
	},
	finish: {
		font: secondFontName,
		fill: fontStrokeColor,
		stroke: fontColor,
		fontSize: 60*ratio,
		strokeThickness: 3*ratio
	},
	timeFall: {
		font: secondFontName,
		fontSize: 20*ratio,
		fill: '#1a6345',
		stroke: '#001708',
		strokeThickness: 3*ratio,
		align: 'right',
		boundsAlignH: 'right'
	},
	stateTitle: {
		font: secondFontName,
		fill: fontColor,
		stroke: fontStrokeColor,
		fontSize: 70*ratio,
		strokeThickness: 5*ratio
	},
	clasificationGroupTitle: {
		font: secondFontName,
		fontSize: 35*ratio,
		fill: fontColor,
		stroke: fontStrokeColor,
		strokeThickness: 2*ratio
	},
	clasificationName: {
		font: secondFontName,
		fill: fontColor,
		stroke: fontStrokeColor,
		fontSize: 20*ratio,
		strokeThickness: 2*ratio
	},
	clasificationRecord: {
		font: secondFontName,
		fill: fontColor,
		stroke: '#c90202',
		fontSize: 22*ratio,
		strokeThickness: 2*ratio,
		align: 'center'
	},
	clasificationDate: {
		font: secondFontName,
		fill: fontColor,
		stroke: fontStrokeColor,
		fontWeight: 'Normal',
		fontSize: 15*ratio,
		strokeThickness: 1*ratio,
		align: 'center'
	},
	record: {
		font: secondFontName,
		fill: fontColor,
		stroke: fontStrokeColor,
		fontSize: 22*ratio,
		strokeThickness: 2*ratio,
		align: 'center'
	},
	recordInput: {
		font: '20px Arial',
		fill: fontStrokeColor,
		fontWeight: 'bold',
		// fontSize: 20,
		width: 250*ratio,
		height: 25*ratio,
		padding: 5*ratio,
		borderWidth: 3*ratio,
		borderColor: fontColor,
		borderRadius: 5,
		backgroundColor: '#84A4FF',
		max: "14"
	},
	settings: {
		font: secondFontName,
		stroke: fontColor,
		fill: fontStrokeColor,
		fontSize: 45*ratio,
		strokeThickness: 2*ratio
	}
};

const timeFall = {
	sizes: {
		min: 30*ratio,
		max: 70*ratio
	},
	values: [
		{
			value: -2,
			color: '#FF0000'
		},
		{
			value: -1.5,
			color: '#FF0000'
		},
		{
			value: -1,
			color: '#FF7000'
		},
		{
			value: 1,
			color: '#23CD00'
		},
		{
			value: 2,
			color: '#01EA87'
		},
		{
			value: 3,
			color: '#25FFED'
		}
	],
	gravity: {
		min: 100,
		max: 2000
	},
	delay: {
		min: 2500,
		max: 8000
	}
};

let globalScoresURL = ( DEBUG ) ? "http://localhost:3002/scores/" : "https://ninjaclimber.urbieta.eus/api/scores/";
// EMAC6 Ajax request
let promiseRequest = (obj) => {
	return new Promise( ( resolve, reject )=>{
		let xhr = new XMLHttpRequest();
		xhr.open( obj.method || "GET", obj.url );
		if ( obj.headers ) {
			Object.keys( obj.headers ).forEach( headerKey => {
				xhr.setRequestHeader( headerKey, obj.headers[ headerKey ] );
			});
		}
		xhr.onload = () => {
			if ( xhr.status >= 200 && xhr.status < 300 ) {
				resolve( xhr.response );
			} else {
				reject( xhr.statusText );
			}
		};
		xhr.onerror = () => reject( xhr.statusText );
		xhr.send( obj.body );
	});
};
