'use strict';

var climber = climber || {};

climber.game = new Phaser.Game(targetWidth, targetHeight, Phaser.CANVAS, 'NinjaClimber');
climber.game.states = {
	BOOT: 'Boot',
	PRELOAD: 'Preload',
	GAME: 'Game',
	MAINMENU: 'MainMenu',
	SCORES: 'Scores',
	SETTINGS: 'Settings'
}
climber.game.state.add( climber.game.states.BOOT, 		climber.Boot);//general game settings are defined
climber.game.state.add( climber.game.states.PRELOAD, 	climber.Preload);//the game assets (images, spritesheets, audio, textures, etc) are loaded into the memory (from the disk). The preloading screen is shown to the user, which usually includes a loading bar to show the progress.
climber.game.state.add( climber.game.states.GAME, 		climber.Game);//your game’s welcome screen. After the preload state, all the game images are already loaded into the memory, so they can quickly accessed.
climber.game.state.add( climber.game.states.MAINMENU, 	climber.MainMenu);
climber.game.state.add( climber.game.states.SCORES, 	climber.Scores);
climber.game.state.add( climber.game.states.SETTINGS, 	climber.Settings);
climber.game.state.start( climber.game.states.BOOT);//the actual game where the FUN takes place.
