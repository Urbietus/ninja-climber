'use strict';

var climber = climber || {};

climber.Preload = function() {};
climber.Preload.prototype = {
	init: function(){
		this.game.load = new CustomLoader( this.game );
		this.game.load.onLoadStart.add( this.createLoadBar, this);
		this.game.load.onFileComplete.add( this.fileComplete, this);
		this.game.load.onLoadComplete.add( this.loadComplete, this);
	},
	createLoadBar: function(){
		var styles = {
			font: '32px Arial',
			fill: fontColor,
			align: 'center'
		};
		this.progressBar = this.game.add.text( this.game.world.centerX, this.game.world.centerY, 'Loading - 0%', styles);
		this.progressBar.anchor.set(0.5);
	},
	fileComplete: function( progress, cacheKey, success, totalLoaded, totalFiles ){
		this.progressBar.text = `Loading - ${progress}%`;
	},
	loadComplete: function(){
		setTimeout(	this.state.start(climber.game.states.MAINMENU), 3000);
	},
	preload: function(){
		//WebFonts
		this.game.load.webfont("fancy",secondFontName);
		this.game.load.webfont("fancy",mainFontName);
		// Plugins
		this.game.add.plugin(Fabrique.Plugins.InputField);
		// Load game files
		this.game.load.image('sky', 'assets/img/clouds2x.jpg');
		this.game.load.image('logo', 'assets/img/logo.png');
		this.game.load.spritesheet('ninja', 'assets/img/ninja.png', 330, 466);
		this.game.load.spritesheet('platform', 'assets/img/platform.png', 64, 64, 7);
		this.game.load.spritesheet('buttons', 'assets/img/buttons.png', 181, 181);
		// Audio
		this.game.audio = {};
		this.game.audio.keys = {
			MUSIC: 'music',
			TIMEADD: 'addTime',
			TIMEREMOVE: 'restTime',
			OPENPARACA: 'openParaca',
			CLOSEPARACA: 'closeParaca',
			JUMP: 'jump',
			LAND: 'land',
			SLICING: 'slicing'
		};
		this.game.load.audio(this.game.audio.keys.MUSIC 		, 'assets/sfx/game_audio.mp3' 	, true);
		this.game.load.audio(this.game.audio.keys.TIMEADD 		, 'assets/sfx/add_time.ogg' 	, true);
		this.game.load.audio(this.game.audio.keys.TIMEREMOVE 	, 'assets/sfx/rest_time.ogg' 	, true);
		this.game.load.audio(this.game.audio.keys.OPENPARACA 	, 'assets/sfx/open_p.ogg' 		, true);
		this.game.load.audio(this.game.audio.keys.CLOSEPARACA 	, 'assets/sfx/close_p.ogg' 		, true);
		this.game.load.audio(this.game.audio.keys.JUMP 			, 'assets/sfx/jump.ogg' 		, true);
		this.game.load.audio(this.game.audio.keys.LAND 			, 'assets/sfx/land.ogg' 		, true);
		this.game.load.audio(this.game.audio.keys.SLICING 		, 'assets/sfx/slicing.ogg' 		, true);

		const SETTINGS_FILENAME = 'NinjaClimber.SETTINGS';
		this.game.settings = JSON.parse( localStorage.getItem( SETTINGS_FILENAME ) )
			||
			{
				FILENAME: SETTINGS_FILENAME,
				music: true,
				effects: true
			};
	},
	create: function(){
		this.game.audio.game = this.game.add.audio( this.game.audio.keys.MUSIC );
		this.game.audio.addTime = this.game.add.audio( this.game.audio.keys.TIMEADD );
		this.game.audio.restTime = this.game.add.audio( this.game.audio.keys.TIMEREMOVE);
		this.game.audio.openParaca = this.game.add.audio( this.game.audio.keys.OPENPARACA);
		this.game.audio.closeParaca = this.game.add.audio( this.game.audio.keys.CLOSEPARACA );
		this.game.audio.jump = this.game.add.audio( this.game.audio.keys.JUMP );
		this.game.audio.land = this.game.add.audio( this.game.audio.keys.LAND );
		this.game.audio.slicing = this.game.add.audio( this.game.audio.keys.SLICING );

		this.game.buttonSize = 110*ratio;

		this.game.camera.quarterHeigth = this.game.camera.height/4;

		this.game.load.start();
	}
}

climber.createAutoScrollBackground = function(){
	this.background = this.game.add.tileSprite(0,0, this.game.width, this.game.height, 'sky');
	this.background.autoScroll( 0, -20);
}

climber.loadGameAudio = function(){
	if( this.game.settings.music && !this.game.audio.game.isPlaying){
		this.game.audio.game.loopFull( 0.4 );
	}
}
climber.playAudio = function( audioKey, loop = false ){
	if( this.game.settings.effects && !this.game.audio[ audioKey ].isPlaying ){
		( loop ) ? this.game.audio[ audioKey ].loopFull( 0.4) : this.game.audio[ audioKey ].play();
	}
}
climber.stopAudio = function( audioKey ){
											if( this.game.settings.effects ){
												this.game.audio[ audioKey ].stop();
											}
										}
climber.buttonsUpEfect = ( sprite )=>{
				sprite.y -= 10;
				sprite.blendMode = PIXI.blendModes.NORMAL;
			}
climber.buttonsDownEfect = ( sprite )=>{
				sprite.y += 10 ;
				sprite.blendMode = PIXI.blendModes.DIFFERENCE;
			}

// We create our own custom loader class extending Phaser.Loader.
// This new loader will support web fonts from https://hacks.mozilla.org/2016/06/webfont-preloading-for-html5-games/
function CustomLoader(game) {
    Phaser.Loader.call(this, game);
}

CustomLoader.prototype = Object.create(Phaser.Loader.prototype);
CustomLoader.prototype.constructor = CustomLoader;

// new method to load web fonts
// this follows the structure of all of the file assets loading methods
CustomLoader.prototype.webfont = function (key, fontName, overwrite) {
    if (typeof overwrite === 'undefined') { overwrite = false; }

    // here fontName will be stored in file's `url` property
    // after being added to the file list
    this.addToFileList('webfont', key, fontName);
    return this;
};

CustomLoader.prototype.loadFile = function (file) {
    Phaser.Loader.prototype.loadFile.call(this, file);

    // we need to call asyncComplete once the file has loaded
    if (file.type === 'webfont') {
        var _this = this;
        // note: file.url contains font name
        var font = new FontFaceObserver(file.url);
        font.load(null, 10000).then(function () {
            _this.asyncComplete(file);
        }, function ()  {
            _this.asyncComplete(file, 'Error loading font ' + file.url);
        });
    }
};
