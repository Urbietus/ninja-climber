'use strict';

var climber = climber || {};
const maxScores = 8;
climber.Scores = function(){};
climber.Scores.prototype = {
	init: function( scoredValue ){
		this.scoredValue = scoredValue;
		this.localScoreKey = 'NinjaClimber.Clasifications';
	},
	create: function(){
		this.padding = 20;
		this.deviceClasification = this.game.add.group();this.globalClasification = this.game.add.group();
		climber.createAutoScrollBackground();
		this.createTitle();
		this.buttonsGroup = this.game.add.group();
		this.buttonsGroup.onChildInputUp.add( climber.buttonsUpEfect, this );
		this.buttonsGroup.onChildInputDown.add( climber.buttonsDownEfect, this );
		this.createBackButton();
		this.drawBox();
		this.createScoreSections();
		this.getGlobalResults();
		this.drawDeviceClasification();
		if( this.scoredValue != undefined && this.scoredValue > 0 ){
			this.drawRecordInput( this.scoredValue );
		}
	},
	update: function(){
		if( this.nameInput ){
			this.nameInput.update();
		}
	},
	getDeviceResults: function(){
		let clasification = JSON.parse( localStorage.getItem( this.localScoreKey ) )
			||
			[
				{
					playerName: 'Urbietus',
					value: 			51.2,
					date: 	new Date( '2017-06-27').getTime()
				}
			];
		return clasification.slice(0,maxScores);
	},
	getGlobalResults: function(){
		promiseRequest( { url: globalScoresURL } )
			.then( data => {
				data = JSON.parse( data );
				if ( !data.error ) {
					this.drawGlobalClasification( data );
				} else{
					this.drawErrorClasification( data.error, this.getGlobalClasificationDrawPoint(),this.globalClasification );
				}
			})
			.catch( error => {
				console.log( error );
				this.drawErrorClasification( error, this.getGlobalClasificationDrawPoint(),this.globalClasification );
			});
	},
	saveDeviceResults: function( results ){
		localStorage.setItem( this.localScoreKey, JSON.stringify(results.slice(0,maxScores)));
	},
	saveGlobalResult: function( result ){
		promiseRequest({
			headers: {'Content-Type': 'application/x-www-form-urlencoded' },
			method: 'POST',
			url: globalScoresURL,
			body: "json="+JSON.stringify(result)
		})
		.then( data => {
			let jsonResult = JSON.parse( data);
			if( jsonResult.saved ) {
				this.getGlobalResults();
			} else {
				console.log( `error saving: ${jsonResult}` );
			}
		})
		.catch( error => console.log( error) );
	},
	createTitle: function(){
		this.title = this.game.add.text( this.game.world.centerX, this.game.camera.y, "Scores", styles.stateTitle );
		this.title.fixedToCamera = true;
		this.title.anchor.set( 0.5, 0);
	},
	createBackButton: function(){
		this.backButton = this.game.add.button( this.game.world.centerX, this.game.camera.y + this.game.height, 'buttons' );
		this.backButton.fixedToCamera = true;
		this.backButton.frame = 0;
		this.backButton.width = this.game.buttonSize;
		this.backButton.height = this.game.buttonSize;
		this.backButton.anchor.set( 0.5, 1.1 );

		this.backButton.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					this.game.state.start(climber.game.states.MAINMENU );
				}
			}, this	);

		this.buttonsGroup.add( this.backButton );
	},
	drawBox: function(){
		let y = this.title.y + this.title.height + this.padding;
		let height = this.backButton.y - this.backButton.height - y - this.padding;
		this.scoreBox = this.game.add.graphics( this.padding , y);
		this.scoreBox.beginFill(0x00033A, 0.8);
		this.scoreBox.drawRoundedRect( 0, 0, this.game.width-(this.padding *2), height, this.padding );
		this.scoreBox.endFill();
	},
	createScoreSections: function(){
		this.localTitle = this.game.add.text( this.padding, 0, 'Device', styles.clasificationGroupTitle );
		this.localTitle.anchor.set( 0, 0 );
		this.globalTitle = this.game.add.text( this.game.world.centerX, 0, 'Global', styles.clasificationGroupTitle);
		this.globalTitle.anchor.set( 0,0);
		this.scoreBox.addChild( this.localTitle );
		this.scoreBox.addChild( this.globalTitle );
	},
	drawRecordInput: function( value){
		const inputWidth = 250*ratio;
		const inputHeight = 30*ratio;
		const inputHalf = (inputWidth/2);
		const margin = 15*ratio;
		let y = this.scoreBox.height - inputHeight - this.padding;

		this.recordInputGroup = this.game.add.group();

		this.nameInput = this.game.add.inputField( this.game.world.centerX-inputHalf, y, styles.recordInput );
		this.recordInputGroup.add( this.nameInput );

		var record = this.game.add.text( this.nameInput.x-margin, y, `${value}m`, styles.record );
		record.anchor.set( 1,0);
		this.recordInputGroup.add( record );

		this.saveButton = this.game.add.button( this.nameInput.x+inputWidth+margin, y, "buttons" );
		this.saveButton.frame = 9;
		this.saveButton.width = 45*ratio;
		this.saveButton.height = 45*ratio;
		this.saveButton.onInputUp.add(
			(sprite, pointer, isOver )=>{
				if( isOver){
					let res = this.getDeviceResults();
					let resObj = {
						playerName: this.nameInput.value,
						value:			this.scoredValue,
						date: 	(new Date).getTime()
					};
					res.push( resObj );
					res.sort( (a,b)=>{ return b.value - a.value } );
					this.recordInputGroup.removeAll( true);
					this.saveGlobalResult( resObj);
					this.saveDeviceResults( res);
					this.drawDeviceClasification();
				}
			}, this	);
		this.saveButton.onInputUp.add( climber.buttonsUpEfect, this );
		this.saveButton.onInputDown.add( climber.buttonsDownEfect, this );
		this.recordInputGroup.add( this.saveButton );
		this.scoreBox.addChild( this.recordInputGroup );
	},
	getGlobalClasificationDrawPoint: function(){
		var drawPoint = {
			x: this.globalTitle.x,
			y: this.globalTitle.y + this.globalTitle.height
		};
		return drawPoint;
	},
	drawGlobalClasification: function( clasification){
		this.drawClasificationList( clasification, this.getGlobalClasificationDrawPoint(), this.globalClasification );
	},
	getDeviceClasificationDrawPoint: function(){
		var drawPoint = {
			x: this.localTitle.x,
			y: this.localTitle.y + this.localTitle.height
		};
		return drawPoint;
	},
	drawDeviceClasification: function(){
		let clasification = this.getDeviceResults();
		this.drawClasificationList( clasification, this.getDeviceClasificationDrawPoint(), this.deviceClasification );
	},
	drawClasificationList: function( clasification, drawPoint, clasificationGroup ){
		this.removeClasificationGroup( clasificationGroup );
		clasification.forEach( (currentValue, index, array )=>{
			let name = this.game.add.text( drawPoint.x, drawPoint.y, `${index+1}.- ${currentValue.playerName}`, styles.clasificationName );
			drawPoint.y = name.y + name.height - 5;
			let value =  this.game.add.text( name.x, drawPoint.y, `${currentValue.value}m`, styles.clasificationRecord );
			value.anchor.set( 0 );
			let date = this.game.add.text(value.x+value.width, drawPoint.y, `- ${ new Date(currentValue.date).toLocaleDateString() }`, styles.clasificationDate );
			date.anchor.set(0, -0.2 );
			drawPoint.y = value.y + value.height;
			clasificationGroup.add( name );
			clasificationGroup.add( value );
			clasificationGroup.add( date );
		});

		this.scoreBox.addChild( clasificationGroup );
	},
	removeClasificationGroup: function( clasificationGroup ){
		if( clasificationGroup ){
			this.scoreBox.removeChild( clasificationGroup );
			clasificationGroup.removeAll( true);
		}
		clasificationGroup = this.game.add.group();
	},
	drawErrorClasification: function( errorMessage, drawPoint, clasificationGroup ){
		this.removeClasificationGroup( clasificationGroup );
		let errorText = this.game.add.text( drawPoint.x,drawPoint.y, "Can not get data", styles.clasificationDate );
		clasificationGroup.add( errorText );
		this.scoreBox.addChild( clasificationGroup );
	}
}
