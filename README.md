# [Ninja climber](https://ninjaclimber.urbieta.eus)

Proyecto para segundo año del Ciclo de oferta parcial de **Técnico Superior en desarrollo de aplicaciones multiplataforma** en el centro de estudios **AEG de Donostia**.

Juego realizado con la librería de Phaser V.2.8 y escrito entre EMACS5 y EMACS6 para su práctica.[ Intención de migrarlo completamente a EMAC6 orientadolo completament a objetos ].
Tambien compilado con cordova para android, en breve lo pondre en la playstore para que podais probarlo.

#### Recursos utilizados:
 * Personaje ninja y Logo: http://www.gameart2d.com/ninja-adventure---free-sprites.html , los he retocado un poco para mis personalización.
 * Sprites suelo: Kenney de, https://opengameart.org/content/platformer-tiles, solo he usado unas pocas pero en src esta toda la libreria.
 * Audios: tambien obtenidos de https://opengameart.org, con licencias CC. ( No encuentro los enlaces lo siento )
 * Botones: Partiendo de https://www.vecteezy.com/vector-art/120177-free-arcade-button-icons-vector, personalizandolos un poco.

Para la automatización de algunos procesos he usado gulp y unos cuantos plugins de gulp y nodeJS.

Gracias a toda la comunidad por compartir todas esas pedazo de librerias y audios que hay por esas webs.

Es mi primer contacto con el desarrollo de juegos(físicas, animaciones...) y espero que no sea el ultimo.
Se aceptan recomendaciones y correcciones.
Muchas gracias.