<?php
abstract class API{
  protected $httpMethod = '';
  protected $endpoint = '';
  protected $verb = '';
  protected $args = [];

  public function __construct( $request ){
    $this->_setHeaders();
    $this->_getArgsFromRequest( $request );
    $this->_readMethod();
  }

  public function processAPI(){
    if ( method_exists( $this, $this->endpoint ) ) {
      return $this->response( $this->{$this->endpoint}($this->args) );
    }
    return $this->response( "No Endpoint: $this->endpoint", StatusCode::NOT_FOUND );
  }

  protected function isHTTPS(){
    return array_key_exists( ServerHeader::HTTPS, $_SERVER );
  }

  protected function response( $data, $status = StatusCode::OK ){
    header("HTTP/1.1 {$status} {$this->_requestStatusMessage($status)}");
    echo json_encode( $data );
    die();
  }

  protected function println( $string ){
    file_put_contents( 'php://stderr',  "$string \n");
  }

  private function _setHeaders(){
    header("Access-Control-Allow-Origin: *" );
    header("Access-Control-Allow-Methods: POST, GET");
    header("Content-Type: application/json" );
  }

  private function _getArgsFromRequest( $request){
    $this->args = explode( '/', trim( $request, '/') );
    $this->endpoint = array_shift( $this->args );
    if( count( $this->args ) > 0 ){
      $this->verb = array_shift( $this->args );
    }
  }

  private function _readMethod(){
    $this->httpMethod = $_SERVER[ ServerHeader::REQUEST_METHOD ];

    if ( $this->httpMethod == Methods::POST && array_key_exists( ServerHeader::HTTP_X_HTTP_METHOD, $_SERVER) ) {
      $XMETHOD = $_SERVER[ ServerHeader::HTTP_X_HTTP_METHOD ];
      if ( $XMETHOD == Methods::DELETE ) {
        $this->httpMethod = Methods::DELETE;
      } else if ($XMETHOD == Methods::PUT ) {
        $this->httpMethod = Methods::PUT;
      } else {
        throw new Exception("Unexpected Header");
      }
    }
    $this->_processMethod();
  }

  private function _processMethod(){
    switch ( $this->httpMethod ) {
      case Methods::GET:
        $this->request = $this->_cleanInputs( $_GET );
        break;
      case Methods::POST:
      case Methods::DELETE:
        $contentType = strtolower( $_SERVER[ServerHeader::CONTENT_TYPE] );
        $jsonPostKey = 'json';
        if ( strpos( $contentType, 'application/json') !== false ) {
          $_POST = json_decode( file_get_contents( "php://input" ) );
        } else
        if ( strpos( $contentType, 'application/x-www-form-urlencoded') !== false
              && array_key_exists( $jsonPostKey, $_POST ) ) {
          $_POST = json_decode( $_POST[ $jsonPostKey ] );
        }
        $this->request = $this->_cleanInputs( $_POST );
        break;
      case Methods::PUT:
        $this->request = $this->_cleanInputs( $_GET );
        $this->file = file_get_contents( "php://input" );
        break;
      default:
        $this->response( "Invalid Method", StatusCode::INVALID_METHOD );
        break;
    }
  }

  private function _requestStatusMessage( $code){
    $statusCodeMessages = [
      StatusCode::OK => "OK",
      StatusCode::BAD_REQ => "Bad request",
      StatusCode::UNAUTHORIZED => "Unauthorized",
      StatusCode::FORBIDDEN => "FORBIDDEN",
      StatusCode::NOT_FOUND => "Not found",
      StatusCode::INVALID_METHOD => "Method not allowed",
      StatusCode::SERVER_ERROR => "Internal server error"
    ];

    return ( $statusCodeMessages[$code] ) ? $statusCodeMessages[$code] : $statusCodeMessages[StatusCode::SERVER_ERROR];
  }

  private function _cleanInputs( $data){
    $clean_input = [];
    if ( is_array( $data ) || is_object( $data ) ) {
      foreach ($data as $key => $value) {
        $clean_input[ $key ] = $this->_cleanInputs( $value );
      }
    } else {
        $clean_input = is_string( $data ) ? trim( strip_tags( $data ) ) : $data;
    }
    return $clean_input;
  }

}

class StatusCode{
  const OK              = 200;
  const BAD_REQ         = 400;
  const UNAUTHORIZED    = 401;
  const FORBIDDEN       = 403;
  const NOT_FOUND       = 404;
  const INVALID_METHOD  = 405;
  const SERVER_ERROR    = 500;
}

class ServerHeader{
  const REQUEST_METHOD      = 'REQUEST_METHOD';
  const REQUEST_URI         = 'REQUEST_URI';
  const HTTP_REFERER        = 'HTTP_REFERER';
  const HTTP_X_HTTP_METHOD  = 'HTTP_X_HTTP_METHOD';
  const HTTP_ORIGIN         = 'HTTP_ORIGIN';
  const HTTP_HOST           = 'HTTP_HOST';
  const HTTPS               = 'HTTPS';
  const SERVER_NAME         = 'SERVER_NAME';
  const QUERY_STRING        = 'QUERY_STRING';
  const PATH                = 'PATH_INFO';
  const REMOTE_ADDR         = 'REMOTE_ADDR';
  const CONTENT_TYPE        = 'CONTENT_TYPE';
  const CLIENT_IP           = 'HTTP_CLIENT_IP';
  const X_FORWARDED_FOR     = 'HTTP_X_FORWARDED_FOR';
  const X_FORWARDED         = 'HTTP_X_FORWARDED';
  const FORWARDED           = 'HTTP_FORWARDED';
  const FORWARDED_FOR       = 'HTTP_FORWARDED_FOR';

  static function getClientIP() {
    $ipaddress = 'UNKNOWN';

    if ( isset( $_SERVER[ self::CLIENT_IP ] ) )
        $ipaddress = $_SERVER[ self::CLIENT_IP ];
    else if(isset($_SERVER[ self::X_FORWARDED_FOR ]))
        $ipaddress = $_SERVER[ self::X_FORWARDED_FOR ];
    else if(isset($_SERVER[ self::X_FORWARDED ] ) )
        $ipaddress = $_SERVER[ self::X_FORWARDED ];
    else if(isset($_SERVER[ self::FORWARDED_FOR ] ) )
        $ipaddress = $_SERVER[ self::FORWARDED_FOR ];
    else if(isset($_SERVER[ self::FORWARDED ] ) )
        $ipaddress = $_SERVER[ self::FORWARDED ];
    else if(isset($_SERVER[ self::REMOTE_ADDR ] ) )
        $ipaddress = $_SERVER[ self::REMOTE_ADDR ];

    return $ipaddress;
  }

}

class Methods{
  const GET     = 'GET';
  const POST    = 'POST';
  const PUT     = 'PUT';
  const DELETE  = 'DELETE';
}

?>
