<?php
  require_once 'API.class.php';
  require_once 'Mlab.class.php';

  class NinjaClimberAPI extends API{
    public function __construct( $request, $origin ) {
      parent::__construct( $request);
      if ( ! DEBUG && ! $this->_validOrigin() ) {
        throw new Exception( "Not valid origin or key" );
      }
    }

    protected function scores( $args){
      switch ($this->httpMethod) {
        case Methods::GET:
          $result = $this->_getScoreResults();
          break;
        case Methods::POST:
          $result = $this->_saveScore();
          break;
      }
    }

    private function _validOrigin(){
      return ( array_key_exists( ServerHeader::HTTP_REFERER, $_SERVER ) && in_array( $_SERVER[ ServerHeader::HTTP_REFERER ], Config::VALID_ORIGINS ) )
      || ( !empty($this->verb) && in_array( $this->verb, Config::VALID_APP_KEYS ) );
    }

    private function _saveScore(){
      try {
        $saved = MLab::saveScore( $this->request[ Scores::COL_PLAYER ], $this->request[ Scores::COL_VALUE ], $this->request[ Scores::COL_DATE ], ServerHeader::getClientIP() );
        $this->response( [ 'saved' => $saved ] );
      } catch (Exception $e) {
        $this->_sendError( $e );
      }
    }
    private function _getScoreResults(){
      try {
        $this->response( Mlab::scoredResults() );
      } catch (Exception $e) {
        $this->_sendError( $e );
      }
    }
    private function _sendError( $error ){
      $this->println( "Error: {$error}" );
      throw $e;
    }
  }
 ?>
