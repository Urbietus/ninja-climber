<?php
  require_once 'vendor/autoload.php';
  require_once 'api.config.php';
  use MongoDB\Client as MongoDBClient;
  use MongoDB\Database as MongoDB;

  class Mlab{
    private static $initialized = false;
    private static $db;
    private static $scores;

    private static function initialize(){
      if( ! self::$initialized ){
        $client = new MongoDBClient( "mongodb://" . Config::DBHOST . ":" . Config::DBPORT, [ "username" => Config::DBUSER , "password" => Config::DBPASS, "authSource" => Config::DBNAME ] );
        self::$db = new MongoDB( $client->getManager(), Config::DBNAME );
        self::$scores = self::$db->{Scores::COLLECTION_NAME};
        self::$initialized = true;
      }
    }
    public static function scoredResults(){
      self::initialize();
      $options = [
        'sort' => [Scores::COL_VALUE => -1 ],
        'limit' => 8
      ];
      $result = self::$scores->find([], $options )->toArray();
      return $result;
    }

    public static function saveScore( $playerName, $value, $date, $ip){
      if ( isset( $playerName ) && $playerName != NULL && isset( $value ) && $value != NULL ) {
        self::initialize();
        $date = !isset( $date) ? (time()*1000) : $date;
        $insertScore = self::$scores->insertOne([
          Scores::COL_PLAYER => $playerName,
          Scores::COL_VALUE  => $value,
          Scores::COL_DATE   => $date,
          Scores::COL_IP     => $ip
        ]);
        return $insertScore->getInsertedCount() > 0;
      }
      return false;
    }
  }

  class Scores{
    const COLLECTION_NAME = "scores";
    const COL_PLAYER = "playerName";
    const COL_VALUE  = "value";
    const COL_DATE   = "date";
    const COL_IP     = "ip";
  }
?>
