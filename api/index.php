<?php
  require_once 'NinjaClimberAPI.class.php';

  define( "DEBUG", false);

  if ( !array_key_exists( ServerHeader::HTTP_ORIGIN, $_SERVER ) ) {
    $_SERVER[ ServerHeader::HTTP_ORIGIN ] = $_SERVER[ ServerHeader::SERVER_NAME ];
  }
  $request = ( !array_key_exists( 'request', $_REQUEST ) ) ? $_SERVER[ ServerHeader::REQUEST_URI ] :  $_REQUEST[ 'request' ];
  if ( $request == '/' ) {
    echo "Ninja Climber API V1";
  }
  else {
    try{
      $API = new NinjaClimberAPI( $request, $_SERVER[ ServerHeader::HTTP_ORIGIN ] );
      echo $API->processAPI();
    } catch ( Exception $e ){
      echo json_encode( [ 'error' => $e->getMessage() ] );
    }
  }
 ?>
