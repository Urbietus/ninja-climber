// SRC Directories
var srcDir = './src/';
var srcJSDir = srcDir + 'js/';
var srcSCSSDir = srcDir + 'scss/';
var srcIMGDir = srcDir + 'img/';
var srcSpritesDir = srcIMGDir + 'sprites/';
var srcLIBsDir = srcDir + 'libs/';
var srcSoundsDir = srcDir + 'sounds/';
var srcFontsDir = srcDir + 'fonts/';
var nodeModules = './node_modules/';
// OUT Directories
var outDir = './app/www/';
var outJSDir = outDir + 'js/';
var outCSSDir = outDir + 'css/';
var outFontsDir = outCSSDir + 'fonts/';
var outASSETSDir = outDir + 'assets/';
var outIMGDir = outASSETSDir + 'img/';
// Libraries
var gulp = require('gulp');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
//sass libs
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');// Autoprefix css
//js libs
var eslint = require('gulp-eslint');
var uglify = require('gulp-uglify');//Minificar JSs
var babel = require('gulp-babel');//EMACS converter
//img libs
var spritesmith = require('gulp.spritesmith');
var imageop = require('gulp-image-optimization');
var jimp = require('gulp-jimp');
//browser sync
var browserSync = require('browser-sync');
var gameBS = browserSync.create();
var reload = gameBS.reload;
var apiBS = browserSync.create();

var php = require( 'gulp-connect-php' );

// Tasks definitions
gulp.task('assets', assetsTask);
gulp.task('img', imgsTask);
gulp.task('sprites', spritesTask);
gulp.task('js',jsTasks);
gulp.task('watch', watchTask);
gulp.task('sass', sassTask);
gulp.task('font', fontTask);
gulp.task('html', htmlTask);
gulp.task('serve', browserSyncTask);
gulp.task('php', phpTask );
gulp.task('default', ['js','sass','font','assets','img','html','serve','watch']);

// Task functions
function phpTask(){

	php.server({
		base: 'api',
		port: 8010,
		keepalive: true
	},()=>{
		apiBS.init({
			proxy: '127.0.0.1:8010'
		})
	});
	gulp.watch( 'restAPI/**/*.php').on('change', ()=>{
		apiBS.reload();
	});
}
function sassTask(){
	var files = [ srcSCSSDir + '**/*.scss', srcSCSSDir + '**/*.sass', '!' + srcSCSSDir + '**/partials/*' ];
	return gulp.src( files)
		.pipe(
			sass({
				outputSyle: 'compressed'
			}).on('error', (err)=>{sass.logError;notify(err);})
		)
		.pipe(
			autoprefixer()
				.on('error', onError)
			)
		.pipe(gulp.dest(outCSSDir));
}
function assetsTask(){
	phaserAsset();
	phaserPlugins();
	fontfaceobserver();
	audioAsset();
	ganalytics();
}
function fontfaceobserver(){
	return gulp.src( srcLIBsDir + 'fontfaceobserver.min.js' )
		.pipe( rename({ dirname: '' }) )
		.pipe( gulp.dest( outASSETSDir ) );
}
function audioAsset(){
	var files = [ srcSoundsDir + '**/*', ];
	return gulp.src( files)
			.pipe( rename( { dirname: 'sfx'} ) )
			.pipe( gulp.dest( outASSETSDir ));
}
function ganalytics(){
	var file = srcLIBsDir + 'ganalytics.js';
	return gulp.src( file )
			.pipe( rename({ dirname:''}))
			.pipe( gulp.dest( outJSDir ));
}
function phaserAsset(){
	var files = [ srcLIBsDir + 'phaser/2.8/*', ];

	return gulp.src( files)
			.pipe( rename( { dirname: 'phaser'} ) )
			.pipe( gulp.dest( outASSETSDir ));
}
function phaserPlugins(){
	var files = [ nodeModules + 'phaser-input/build/phaser-input.min.js'];
	return gulp.src( files )
			.pipe(
				rename({
					dirname: 'phaser/plugins'
				})
			)
			.pipe( gulp.dest( outASSETSDir ) );
}
function imgsTask(){
	var files = [ srcIMGDir + '*.png', '!' + srcIMGDir + '*.eps' ];
	return gulp.src( files)
			.pipe( rename({ dirname: '' }) )
			.pipe( gulp.dest( outIMGDir ))
}
function spritesTask(){
	return buttonsSprite();
	//return ninjaSprites();
	// return platformSprites();
	//return trunkSprites();
}
function buttonsSprite(){
	var trunkDir = srcSpritesDir + 'buttons/';
	var trunk = gulp.src( trunkDir + '*.png' )
				.pipe(
					imageop({
						optimizationLevel: 5,
						progressive: true,
						interlaced: true
					})
					.on('error', (err)=>{ notify(err)})
				)
				.pipe(
					spritesmith({
						imgName: 'buttons.png',
						cssName: 'buttons.json',
						algorithm: 'left-right',
						algorithmOpts: { sort: false}
					})
					.on('error', (err)=>{ notify(err)})
				).pipe(
					gulp.dest( outIMGDir)
				);
}
function onError( err){
	console.log( err);
}
function ninjaSprites(){
	var ninjaDir = srcSpritesDir + 'ninja/';
	var ninjaDirAnimations = ninjaDir + 'animations/';

	var idle = gulp.src( ninjaDir + 'Idle*.png' )
				.pipe(
					imageop({
						optimizationLevel: 5,
						progressive: true,
						interlaced: true
					})
					.on('error', (err)=>{ notify(err)})
				)
				.pipe(
					spritesmith({
						imgName: 'Idle.png',
						cssName: 'Idle.json',
						algorithm: 'left-right',
						algorithmOpts: { sort: false}
					})
					.on('error', (err)=>{ notify(err)})
				).pipe(
					gulp.dest( ninjaDirAnimations)
				);

	var jumpleft = gulp.src( ninjaDir + 'Jump_left*.png' )
				.pipe(
					imageop({
						optimizationLevel: 5,
						progressive: true,
						interlaced: true
					})
					.on('error', (err)=>{ notify(err)})
				)
				.pipe(
					spritesmith({
						imgName: 'Jump_left.png',
						cssName: 'Jump_left.json',
						algorithm: 'left-right',
						algorithmOpts: { sort: false}
					})
					.on('error', (err)=>{ notify(err)})

				).pipe(
					gulp.dest( ninjaDirAnimations)
				);

	var jumpright = gulp.src( ninjaDir + 'Jump_right*.png' )
				.pipe(
					imageop({
						optimizationLevel: 5,
						progressive: true,
						interlaced: true
					})
					.on('error', (err)=>{ notify(err)})
				)
				.pipe(
					spritesmith({
						imgName: 'Jump_right.png',
						cssName: 'Jump_right.json',
						algorithm: 'left-right',
						algorithmOpts: { sort: false}
					})
					.on('error', (err)=>{ notify(err)})
				).pipe(
					gulp.dest( ninjaDirAnimations)
				);

	var slideleft = gulp.src( ninjaDir + 'Slide_left*.png' )
				.pipe(
					imageop({
						optimizationLevel: 5,
						progressive: true,
						interlaced: true
					})
					.on('error', (err)=>{ notify(err)})
				)
				.pipe(
					spritesmith({
						imgName: 'Slide_left.png',
						cssName: 'Slide_left.json',
						algorithm: 'left-right',
						algorithmOpts: { sort: false}
					})
					.on('error', (err)=>{ notify(err)})
				).pipe(
					gulp.dest( ninjaDirAnimations)
				);

	var sliderigth = gulp.src( ninjaDir + 'Slide_right*.png' )
				.pipe(
					imageop({
						optimizationLevel: 5,
						progressive: true,
						interlaced: true
					})
					.on('error', (err)=>{ notify(err)})
				)
				.pipe(
					spritesmith({
						imgName: 'Slide_right.png',
						cssName: 'Slide_right.json',
						algorithm: 'left-right',
						algorithmOpts: { sort: false}
					})
					.on('error', (err)=>{ notify(err)})
				).pipe(
					gulp.dest( ninjaDirAnimations)
				);
	var glideRight = gulp.src( ninjaDir + 'Glide*.png' )
		.pipe(
			imageop({
				optimizationLevel: 5,
				progressive: true,
				interlaced: true
			})
			.on('error', (err)=>{ notify(err)})
		)
		.pipe(
			spritesmith({
				imgName: 'zGlide_right.png',
				cssName: 'zGlide_right.json',
				algorithm: 'left-right',
				algorithmOpts: { sort: false}
			})
			.on('error', (err)=>{ notify(err)})
		).pipe(
			gulp.dest( ninjaDirAnimations)
		);
		var glideLeft = gulp.src( ninjaDir + 'Glide*.png' )
		.pipe(
			imageop({
				optimizationLevel: 5,
				progressive: true,
				interlaced: true
			})
			.on('error', (err)=>{ notify(err)})
		)
		.pipe(
			jimp({
				'-left':{
					flip:{
						horizontal: true,
						vertical: false
					}
				}
			})
		)
		.pipe(
			spritesmith({
				imgName: 'zGlide_left.png',
				cssName: 'zGlide_left.json',
				algorithm: 'left-right',
				algorithmOpts: { sort: false}
			})
			.on('error', (err)=>{ notify(err)})
		).pipe(
			gulp.dest( ninjaDirAnimations)
		);

	var allAnimations = gulp.src( ninjaDirAnimations + '*.png' )
		.pipe(
			spritesmith({ imgName: 'ninja.png',
				cssName: 'ninja.json',
				algorithm: 'top-down',
				algorithmOpts: { sort: false }
			})
					.on('error', (err)=>{ notify(err)})
		);

	return allAnimations.img.pipe( gulp.dest( outIMGDir ));
}
function platformSprites(){
	var platformFolder = srcSpritesDir + 'free/Tiles/';
	var colorPlatformFileNameBegin = platformFolder + 'Green tiles/tileGreen_';
	var tileNumbers = [ '04','05','06','09','10','24','25'];
	var tilesFiles = tileNumbers.map( (element)=>{ return `${colorPlatformFileNameBegin}${element}.png`});
	return gulp.src( tilesFiles)
			.pipe(
				imageop({
					optimizationLevel: 5,
					progressive: true,
					interlaced: true
				})
			)
			.pipe(
				spritesmith({
					imgName: 'platform.png',
					cssName: 'platform.json',
					algorithm: 'left-right',
					algorithmOpts: { sort: false}
				})
			).pipe(
				gulp.dest( outIMGDir)
			);
}
function fontTask(){
	var files = [ srcFontsDir + '**/*.ttf' ];
	return gulp.src( files)
			.pipe( rename({ dirname: '' }) )
			.pipe( gulp.dest( outFontsDir ) );
}

function jsTasks(){
	var files = [ srcJSDir + '**/*.js' ];
	return	gulp.src( files )
				.pipe(
					babel({ presets: ['es2015']})
				)
				.pipe(
					eslint({
					    "rules":{
					        "camelcase": 1,
					        "comma-dangle": 2,
					        "quotes": 0
					    }
					})
				)
				.on('error', onError)
				.pipe(
					concat('main.js')
				)
				.on('error', onError)
				.pipe(
					uglify( { compress: true })
				)
				.on('error', onError)
				.pipe(
					gulp.dest( outJSDir )
				);
}

function htmlTask(){
	var files = [srcDir + '**/*.html'];
	return gulp.src( files )
		.pipe( rename({ dirname: '' }) )
		.pipe( gulp.dest( outDir ));
}
function browserSyncTask(){
	gameBS.init({
		server: outDir
	});
}
function watchTask(){
	gulp.watch( srcJSDir + '**/*.js', ['js']).on('change', reload);
	gulp.watch( srcSCSSDir + '**/*.scss', ['sass']).on('change', reload);
	gulp.watch( srcFontsDir + '**/*', ['font']).on('change', reload);
	gulp.watch( srcDir + '**/*.html', ['html']).on('change', reload);
	gulp.watch( srcIMGDir + '**/*', ['img']).on('change', reload);
}
